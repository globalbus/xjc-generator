package info.globalbus.xjc.choice;

import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSWildcard;
import com.sun.xml.xsom.visitor.XSTermVisitor;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class GroupXSTermVisitor implements XSTermVisitor {
    private final Consumer<XSModelGroup> consumer;

    @Override
    public void wildcard(XSWildcard wc) {
        //NOP
    }

    @Override
    public void modelGroupDecl(XSModelGroupDecl decl) {
        //NOP
    }

    @Override
    public void modelGroup(XSModelGroup group) {
        consumer.accept(group);
    }

    @Override
    public void elementDecl(XSElementDecl decl) {
        //NOP
    }
}
