package info.globalbus.xjc.choice;

import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.model.CClassInfo;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSComponent;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.ErrorHandler;

@Slf4j
public class ChoiceValidatorPlugin extends Plugin {

    private static final String OPTION_NAME = "XChoiceValidator";

    @Override
    public String getOptionName() {
        return OPTION_NAME;
    }

    @Override
    public String getUsage() {
        return "-" + OPTION_NAME
               + "    :   Annotates classes with choice elements \n";
    }

    @Override
    public boolean run(Outline outline, Options opt, ErrorHandler errorHandler) {
        for (CClassInfo classInfo : outline.getModel().beans().values()) {
            XSComponent component = classInfo.getSchemaComponent();
            if (component instanceof XSComplexType) {
                new ComplexTypeProcessor(outline, classInfo, (XSComplexType) component).process();
            }
        }
        return true;
    }

}
