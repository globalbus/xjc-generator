package info.globalbus.xjc.choice;

import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.visitor.XSContentTypeVisitor;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class ParticleXSContentTypeVisitor implements XSContentTypeVisitor {
    private final Consumer<XSParticle> consumer;

    @Override
    public void simpleType(XSSimpleType simpleType) {
        //NOP
    }

    @Override
    public void particle(XSParticle particle) {
        consumer.accept(particle);
    }

    @Override
    public void empty(XSContentType empty) {
        //NOP
    }
}
