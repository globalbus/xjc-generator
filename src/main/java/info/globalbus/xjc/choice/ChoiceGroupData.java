package info.globalbus.xjc.choice;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
class ChoiceGroupData {
    int minOccurs;
    int maxOccurs;
    int id;
    List<ChoiceElementData> elements = new ArrayList<>();
}
