package info.globalbus.xjc.choice;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class ChoiceGroupHolder {
    private int id = 0;
    @Getter
    List<ChoiceGroupData> groupDatas = new ArrayList<>();

    public int add(ChoiceGroupData data) {
        data.setId(++id);
        groupDatas.add(data);
        return id;
    }

    public boolean isEmpty() {
        return groupDatas.isEmpty();
    }
}
