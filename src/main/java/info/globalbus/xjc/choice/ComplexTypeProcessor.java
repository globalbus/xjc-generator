package info.globalbus.xjc.choice;

import com.sun.codemodel.JAnnotationArrayMember;
import com.sun.codemodel.JAnnotationUse;
import com.sun.tools.xjc.model.CClassInfo;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSTerm;
import java.util.List;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import pl.warta.soa.libs.validator.constraints.ChoiceElement;
import pl.warta.soa.libs.validator.constraints.ChoiceGroup;
import pl.warta.soa.libs.validator.constraints.ChoiceGroups;

@RequiredArgsConstructor
class ComplexTypeProcessor {
    private final Outline outline;
    private final CClassInfo classInfo;
    private final XSComplexType component;
    private final ChoiceGroupHolder choiceGroupHolder = new ChoiceGroupHolder();

    void process() {
        component.getContentType().visit(new ParticleXSContentTypeVisitor(particle ->
            particle.getTerm().visit(new GroupXSTermVisitor(group -> consumeGroup(particle, group)))
        ));
        if (!choiceGroupHolder.isEmpty()) {
            annotate(choiceGroupHolder.getGroupDatas(), outline.getClazz(classInfo));
        }
    }

    private void consumeGroup(XSParticle particle, XSModelGroup group) {
        if (XSModelGroup.Compositor.CHOICE.equals(group.getCompositor())) {
            getChoiceGroupData(particle, group);
        } else {
            for (XSParticle child : group.getChildren()) {
                final Consumer<XSParticle> particleConsumer = innerParticle -> innerParticle.getTerm()
                    .visit(new GroupXSTermVisitor(innerGroup -> consumeGroup(particle, innerGroup)));
                child.visit(new ParticleXSContentTypeVisitor(particleConsumer));
            }
        }
    }

    private void getChoiceGroupData(XSParticle particle, XSModelGroup group) {
        ChoiceGroupData choiceGroup = new ChoiceGroupData();
        choiceGroup.setMinOccurs(particle.getMinOccurs().intValue());
        choiceGroup.setMaxOccurs(particle.getMaxOccurs().intValue());
        for (XSParticle child : group.getChildren()) {
            ChoiceElementData choiceElement = new ChoiceElementData();
            choiceElement.setMinOccurs(child.getMinOccurs().intValue());
            choiceElement.setMaxOccurs(child.getMaxOccurs().intValue());
            final XSTerm childTerm = child.getTerm();
            if (childTerm instanceof XSElementDecl) {
                choiceElement.setName(findField(child, classInfo));
            } else if (childTerm instanceof XSModelGroup) {
                final XSModelGroup modelGroup = (XSModelGroup) childTerm;
                ChoiceGroupData choiceGroupInner = new ChoiceGroupData();
                int minOccurs = 0;
                int maxOccurs = 0;
                for (XSParticle childInner : modelGroup.getChildren()) {
                    ChoiceElementData choiceElementInner = new ChoiceElementData();
                    choiceElementInner.setMinOccurs(childInner.getMinOccurs().intValue());
                    minOccurs += choiceElementInner.getMinOccurs();
                    choiceElementInner.setMaxOccurs(childInner.getMaxOccurs().intValue());
                    if (choiceElementInner.getMaxOccurs() == -1) {
                        maxOccurs = -1;
                    } else if (maxOccurs != -1) {
                        maxOccurs += choiceElementInner.getMaxOccurs();
                    }
                    if (childInner.getTerm() instanceof XSElementDecl) {
                        choiceElementInner.setName(findField(childInner, classInfo));
                    }
                    choiceGroupInner.getElements().add(choiceElementInner);
                }
                choiceGroupInner.setMinOccurs(minOccurs);
                choiceGroupInner.setMaxOccurs(maxOccurs);
                if (choiceGroupInner.getElements().size() == 1) {
                    choiceElement.setName(choiceGroupInner.getElements().get(0).getName());
                } else {
                    choiceElement.setId(choiceGroupHolder.add(choiceGroupInner));
                }
            }
            choiceGroup.getElements().add(choiceElement);
        }
        choiceGroupHolder.add(choiceGroup);
    }


    private static String findField(XSParticle particle, CClassInfo classInfo) {
        return classInfo.getProperties().stream()
            .filter(v -> v.getSchemaComponent().equals(particle) || v.getSchemaComponent().equals(particle.getTerm()))
            .map(v -> v.getName(false)).findFirst().orElse(null);
    }

    private static void annotate(List<ChoiceGroupData> choiceGroupList, ClassOutline clazz) {
        JAnnotationArrayMember groupArray = clazz.getImplClass().annotate(ChoiceGroups.class).paramArray("elements");
        for (ChoiceGroupData choiceGroup : choiceGroupList) {
            final JAnnotationUse groupAnnotation = groupArray.annotate(ChoiceGroup.class);
            if (choiceGroup.getMinOccurs() != 1) {
                groupAnnotation.param("minOccurs", choiceGroup.getMinOccurs());
            }
            if (choiceGroup.getMaxOccurs() != 1) {
                groupAnnotation.param("maxOccurs", choiceGroup.getMaxOccurs());
            }
            if (choiceGroup.getId() != 0) {
                groupAnnotation.param("id", choiceGroup.getId());
            }
            JAnnotationArrayMember elementArray = groupAnnotation.paramArray("elements");
            for (ChoiceElementData element : choiceGroup.getElements()) {
                final JAnnotationUse elementAnnotation = elementArray.annotate(ChoiceElement.class);
                if (element.getMinOccurs() != 1) {
                    elementAnnotation.param("minOccurs", element.getMinOccurs());
                }
                if (element.getMaxOccurs() != 1) {
                    elementAnnotation.param("maxOccurs", element.getMaxOccurs());
                }
                if (StringUtils.isNotEmpty(element.getName())) {
                    elementAnnotation.param("name", element.getName());
                } else {
                    elementAnnotation.param("id", element.getId());
                }
            }
        }
    }
}
