package info.globalbus.xjc.choice;

import lombok.Data;

@Data
class ChoiceElementData {
    int minOccurs;
    int maxOccurs;
    int id;
    String name;
}
