package info.globalbus.xjc.simplify;

import com.sun.codemodel.JJavaName;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.model.CAdapter;
import com.sun.tools.xjc.model.CAttributePropertyInfo;
import com.sun.tools.xjc.model.CClassInfo;
import com.sun.tools.xjc.model.CClassRef;
import com.sun.tools.xjc.model.CElement;
import com.sun.tools.xjc.model.CElementInfo;
import com.sun.tools.xjc.model.CElementPropertyInfo;
import com.sun.tools.xjc.model.CElementPropertyInfo.CollectionMode;
import com.sun.tools.xjc.model.CPropertyInfo;
import com.sun.tools.xjc.model.CPropertyVisitor;
import com.sun.tools.xjc.model.CReferencePropertyInfo;
import com.sun.tools.xjc.model.CTypeRef;
import com.sun.tools.xjc.model.CValuePropertyInfo;
import com.sun.tools.xjc.model.Model;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.bind.v2.model.core.ID;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.ErrorHandler;

@Slf4j
public class SimplifyPlugin extends Plugin {

    @Getter
    private boolean usePluralForm = true;


    @Override
    public String getOptionName() {
        return "Xsimplify";
    }

    @Override
    public String getUsage() {
        return "This plugin allows simplifying \"complex\" properties"
            + " (ex. aOrBOrC generated from repeatable choices)"
            + " into several \"simple\" properties (ex. a, b, c).\n";
    }

    @Override
    public void postProcessModel(final Model model, ErrorHandler errorHandler) {
        for (final CClassInfo classInfo : model.beans().values()) {
            postProcessClassInfo(model, classInfo);
        }
    }

    @Override
    public boolean run(Outline outline, Options opt, ErrorHandler errorHandler) {
        return true;
    }

    private CReferencePropertyInfo createContentReferencePropertyInfo(CReferencePropertyInfo property) {
        final String propertyName = "Mixed" + property.getName(true);
        return new CReferencePropertyInfo(
            propertyName, /* collection */true, /* required */false, /* mixed */
            true, property.getSchemaComponent(),
            property.getCustomizations(), property.getLocator(), false,
            true, property.isMixedExtendedCust());
    }

    private CElementPropertyInfo createElementPropertyInfo(final Model model,
                                                           CReferencePropertyInfo property, CElement element,
                                                           final CElementInfo elementInfo) {
        final CElementPropertyInfo elementPropertyInfo;
        final String propertyName = createPropertyName(model, property, element);
        final CElementPropertyInfo originalPropertyInfo = elementInfo
            .getProperty();
        elementPropertyInfo = new CElementPropertyInfo(propertyName,
            property.isCollection() ? CollectionMode.REPEATED_ELEMENT
                : CollectionMode.NOT_REPEATED, ID.NONE, null,
            element.getSchemaComponent(), element.getCustomizations(),
            element.getLocator(), false);

        final CAdapter adapter = originalPropertyInfo.getAdapter();
        if (adapter != null) {
            elementPropertyInfo.setAdapter(adapter);
        }

        elementPropertyInfo.getTypes().add(
            new CTypeRef(elementInfo.getContentType(), element
                .getElementName(), elementInfo.getContentType()
                .getTypeName(), false, null));
        return elementPropertyInfo;
    }

    private CElementPropertyInfo createElementPropertyInfo(final Model model,
                                                           CReferencePropertyInfo property, CElement element,
                                                           final CClassInfo classInfo) {
        final CElementPropertyInfo elementPropertyInfo;
        final String propertyName = createPropertyName(model, property, element);
        elementPropertyInfo = new CElementPropertyInfo(propertyName,
            property.isCollection() ? CollectionMode.REPEATED_ELEMENT
                : CollectionMode.NOT_REPEATED, ID.NONE, null,
            element.getSchemaComponent(), element.getCustomizations(),
            element.getLocator(), false);
        elementPropertyInfo.getTypes().add(
            new CTypeRef(classInfo, element.getElementName(), classInfo
                .getTypeName(), false, null));
        return elementPropertyInfo;
    }

    private String createPropertyName(final Model model,
                                      CPropertyInfo propertyInfo, CElement element) {
        final String localPart;
        if (element instanceof CClassRef) {
            final CClassRef classRef = (CClassRef) element;
            final String fullName = classRef.fullName();
            localPart = fullName.substring(fullName.lastIndexOf('.') + 1);
        } else {
            localPart = element.getElementName().getLocalPart();
        }
        return model.getNameConverter().toPropertyName(
            pluralizeIfNecessary(propertyInfo, localPart));
    }

    private String pluralizeIfNecessary(CPropertyInfo propertyInfo,
                                        final String propertyName) {
        return (propertyInfo.isCollection() && isUsePluralForm()) ? JJavaName
            .getPluralForm(propertyName) : propertyName;
    }

    private void postProcessClassInfo(final Model model,
                                      final CClassInfo classInfo) {
        final List<CPropertyInfo> properties = new ArrayList<>(
            classInfo.getProperties());
        for (CPropertyInfo property : properties) {
            property.accept(new CPropertyVisitor<Void>() {

                public Void onAttribute(CAttributePropertyInfo attributeProperty) {
                    return null;
                }

                public Void onElement(CElementPropertyInfo elementProperty) {
                    return null;
                }

                public Void onReference(CReferencePropertyInfo p) {
                    simplifyReferencePropertyInfoAsElementPropertyInfo(model,
                        classInfo, p);
                    return null;
                }

                public Void onValue(CValuePropertyInfo valueProperty) {
                    return null;
                }

            });
        }
    }


    private void simplifyReferencePropertyInfoAsElementPropertyInfo(
        final Model model, final CClassInfo classInfo,
        CReferencePropertyInfo property) {

        if (property.getElements().size() <= 1 && !property.isMixed()) {
            log.warn(MessageFormat
                .format("Element reference property [{0}] will not be simplified as it does not contain multiple elements and is not mixed.",
                    property.getName(false)));
        } else {
            log.debug(MessageFormat
                .format("Element reference property [{0}] contains multiple elements or is mixed and will be simplified.",
                    property.getName(false)));
            int index = classInfo.getProperties().indexOf(property);
            for (CElement element : property.getElements()) {
                final CElementPropertyInfo elementPropertyInfo;
                if (element instanceof CElementInfo) {
                    elementPropertyInfo = createElementPropertyInfo(model,
                        property, element, (CElementInfo) element);
                } else if (element instanceof CClassInfo) {
                    elementPropertyInfo = createElementPropertyInfo(model,
                        property, element, (CClassInfo) element);

                } else if (element instanceof CClassRef) {
                    log.error(MessageFormat
                        .format("Element reference property [{0}] contains a class reference type [{1}] and therefore cannot be fully simplified as element property.",
                            property.getName(false),
                            ((CClassRef) element).fullName()));
                    elementPropertyInfo = null;
                } else {
                    elementPropertyInfo = null;
                    log.error(MessageFormat.format(
                        "Unsupported CElement type [{0}].", element));
                }
                if (elementPropertyInfo != null) {
                    classInfo.getProperties().add(index++, elementPropertyInfo);
                }
            }
            if (property.isMixed()) {
                classInfo.getProperties().add(index++,
                    createContentReferencePropertyInfo(property));
            }
            classInfo.getProperties().remove(property);
        }
    }
}
