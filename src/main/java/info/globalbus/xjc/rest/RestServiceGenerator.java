package info.globalbus.xjc.rest;

import com.sun.codemodel.ClassType;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.api.ClassNameAllocator;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BindInfo;
import com.sun.xml.xsom.XSAnnotation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.tools.util.ClassCollector;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

@Slf4j
public class RestServiceGenerator extends Plugin {
    private static final String CHANNEL_EXCEPTION = "ChannelException";
    private static final String OPTION_NAME = "XRestServiceGenerator";
    private static final String CMM_PACKAGE_PARAMETER_NAME = OPTION_NAME + ":cmmPackage";
    private static final String VALUE = "value";
    private String servicePackage;
    private String exceptionPackage;

    @Override
    public String getOptionName() {
        return OPTION_NAME;
    }

    @Override
    public String getUsage() {
        return "-" + OPTION_NAME
               + "    :   Generates Rest Service Interface \n"
               + "-" + CMM_PACKAGE_PARAMETER_NAME
               + "    :   root package for service name classes\n";
    }

    @Override
    public int parseArgument(Options opt, String[] args, int i) {
        String arg1 = args[i];
        int consumed = 0;
        int index = arg1.indexOf(CMM_PACKAGE_PARAMETER_NAME);
        if (index > 0) {
            final String aPackage = arg1.substring(index + CMM_PACKAGE_PARAMETER_NAME.length() + 1);
            servicePackage = aPackage + ".service";
            exceptionPackage = aPackage + ".exception";
            consumed++;
        }
        return consumed;
    }

    @Override
    public boolean run(Outline outline, Options opt, ErrorHandler errorHandler) throws SAXException {
        try {
            ClassNameAllocator classNameAllocator = opt.classNameAllocator;
            Field collectorField = classNameAllocator.getClass().getDeclaredField("collector");
            collectorField.setAccessible(true);
            ClassCollector collector = (ClassCollector) collectorField.get(classNameAllocator);
            for (String sei : collector.getSeiClassNames().values()) {
                generateServiceResource(outline, sei);
            }
        } catch (Exception ex) {
            log.error("Cannot generate rest service", ex);
            throw new SAXException(ex);
        }
        return true;
    }

    private boolean compatible(ClassOutline clazz, String packageName) {
        ClassOutline superClass = getRootClass(clazz);
        return superClass != null
               && superClass.getImplClass().getPackage().name().equals(servicePackage)
               && clazz.getImplClass().getPackage().name().equals(packageName);
    }

    private ClassOutline getRootClass(ClassOutline clazz) {
        ClassOutline rootClazz = clazz;
        while (rootClazz.getSuperClass() != null) {
            rootClazz = rootClazz.getSuperClass();
        }
        if (rootClazz == clazz) {
            return null;
        }
        return rootClazz;
    }

    private void generateServiceResource(Outline outline, String sei) throws JClassAlreadyExistsException {
        int indexOf = sei.lastIndexOf('.');
        String packageName = sei.substring(0, indexOf);
        String serviceName = StringUtils.removeEnd(sei.substring(indexOf + 1), "PortType");
        List<ClassOutline> serviceObjects = outline.getClasses().stream()
            .filter(clazz -> compatible(clazz, packageName)).collect(Collectors.toList());
        Map<String, ClassOutline> requests = serviceObjects.stream()
            .filter(v -> v.getImplClass().name().endsWith("Request"))
            .collect(Collectors.toMap(v -> v.getImplClass().fullName(), Function.identity()));
        Map<String, ClassOutline> responses = serviceObjects.stream()
            .filter(v -> v.getImplClass().name().endsWith("Response"))
            .collect(Collectors.toMap(v -> v.getImplClass().fullName(), Function.identity()));
        JCodeModel codeModel = outline.getCodeModel();
        JDefinedClass resource = codeModel._class(packageName + "." + serviceName + "Resource", ClassType.INTERFACE);
        JDefinedClass operationNames = codeModel._class(JMod.PUBLIC | JMod.FINAL,
            packageName + "." + serviceName + "OperationNames", ClassType.CLASS);
        operationNames.annotate(UtilityClass.class);
        resource.annotate(Api.class).param(VALUE, "/");
        resource.annotate(Path.class).param(VALUE, "/");
        for (Map.Entry<String, ? extends ClassOutline> entry : responses.entrySet()) {
            JDefinedClass implClass = entry.getValue().getImplClass();
            String operationName = StringUtils.removeEnd(implClass.name(), "Response");
            final String uncapitalizedOperationName = StringUtils.uncapitalize(operationName);
            String requestClassName = packageName + "." + operationName + "Request";
            ClassOutline request = requests.get(requestClassName);
            if (request == null) {
                continue;
            }
            operationNames.field(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, String.class, toSnakeCase(operationName))
                .init(JExpr.lit(uncapitalizedOperationName));
            JMethod method = resource.method(JMod.NONE, implClass, uncapitalizedOperationName);
            method.param(request.getImplClass(), "request");
            method.annotate(POST.class);
            method.annotate(Path.class).param(VALUE, "/" + uncapitalizedOperationName);
            method.annotate(Consumes.class).param(VALUE, "application/json");
            method.annotate(Produces.class).param(VALUE, "application/json");
            method.annotate(ApiResponses.class).annotationParam(VALUE, ApiResponse.class)
                .param("code", 500).param("message", CHANNEL_EXCEPTION)
                .param("response", codeModel.directClass(exceptionPackage + "." + CHANNEL_EXCEPTION));
            String desc = Optional.ofNullable(request.getTarget().getSchemaComponent().getAnnotation())
                .map(XSAnnotation::getAnnotation).map(BindInfo.class::cast).map(BindInfo::getDocumentation)
                .orElse("No description");
            method.annotate(ApiOperation.class).param(VALUE, desc);
        }
    }

    private String toSnakeCase(String operationName) {
        return Stream.of(StringUtils.splitByCharacterTypeCamelCase(operationName)).map(String::toUpperCase)
            .collect(Collectors.joining("_"));
    }
}