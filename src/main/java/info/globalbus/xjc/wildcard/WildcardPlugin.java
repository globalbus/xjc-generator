package info.globalbus.xjc.wildcard;

import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.model.CClassInfo;
import com.sun.tools.xjc.model.CPropertyInfo;
import com.sun.tools.xjc.model.CReferencePropertyInfo;
import com.sun.tools.xjc.model.Model;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.bind.v2.model.core.WildcardMode;
import org.xml.sax.ErrorHandler;

public class WildcardPlugin extends Plugin {

    @Override
    public String getOptionName() {
        return "Xwildcard";
    }

    @Override
    public String getUsage() {
        return "Set wildcard mode to strict";
    }

    @Override
    public void postProcessModel(Model model, ErrorHandler errorHandler) {
        for (CClassInfo classInfo : model.beans().values()) {
            for (CPropertyInfo propertyInfo : classInfo.getProperties()) {
                if (propertyInfo instanceof CReferencePropertyInfo) {
                    final CReferencePropertyInfo referencePropertyInfo = (CReferencePropertyInfo) propertyInfo;
                    referencePropertyInfo.setWildcard(WildcardMode.STRICT);
                }
            }
        }
    }

    @Override
    public boolean run(Outline outline, Options opt, ErrorHandler errorHandler) {
        return false;
    }
}
