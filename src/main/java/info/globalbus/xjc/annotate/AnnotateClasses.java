package info.globalbus.xjc.annotate;

import com.sun.codemodel.JFieldVar;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.generator.bean.BeanGenerator;
import com.sun.tools.xjc.generator.bean.field.SingleField;
import com.sun.tools.xjc.model.CPropertyInfo;
import com.sun.tools.xjc.outline.FieldOutline;
import com.sun.tools.xjc.outline.Outline;
import com.sun.xml.xsom.XSDeclaration;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.impl.ParticleImpl;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

@Slf4j
public class AnnotateClasses extends Plugin {

    private static final String OPTION_NAME = "XAnnotate";
    private static final String CMM_NAMESPACE_PARAMETER_NAME = OPTION_NAME + ":namespace";
    private static final String CMM_CONSTRAINT_PARAMETER_NAME = OPTION_NAME + ":constraint";
    private final Map<String, String> annotations = new HashMap<>();
    private final Field getFields;
    private String namespace;

    private final Field getFieldImpl;


    public AnnotateClasses() {
        try {
            getFields = BeanGenerator.class.getDeclaredField("fields");
            getFields.setAccessible(true);
            getFieldImpl = SingleField.class.getSuperclass().getDeclaredField("field");
            getFieldImpl.setAccessible(true);
        } catch (Exception ex) {
            log.error("Cannot get reflection", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String getOptionName() {
        return OPTION_NAME;
    }

    @Override
    public String getUsage() {
        return "-" + OPTION_NAME
               + "    :   Annotate fields\n";
    }

    @Override
    public int parseArgument(Options opt, String[] args, int i) {
        String arg1 = args[i];
        int consumed = 0;
        int index = arg1.indexOf(CMM_NAMESPACE_PARAMETER_NAME);
        if (index > 0) {
            namespace = arg1.substring(index + CMM_NAMESPACE_PARAMETER_NAME.length() + 1);
            consumed++;
        }
        index = arg1.indexOf(CMM_CONSTRAINT_PARAMETER_NAME);
        if (index > 0) {
            String constraint = arg1.substring(index + CMM_CONSTRAINT_PARAMETER_NAME.length() + 1);
            int singleIndex = constraint.indexOf('=');
            if (singleIndex > 0) {
                String name = constraint.substring(0, singleIndex);
                String className = constraint.substring(singleIndex + 1, constraint.length());
                annotations.put(name, className);
            }
            consumed++;
        }
        return consumed;
    }

    @Override
    public boolean run(Outline outline, Options opt, ErrorHandler errorHandler) throws SAXException {
        try {
            Map<CPropertyInfo, FieldOutline> fields = (Map<CPropertyInfo, FieldOutline>) getFields.get(outline);
            for (Map.Entry<CPropertyInfo, FieldOutline> fieldEntry : fields.entrySet()) {
                final Optional<XSType> xsType = Optional.ofNullable(fieldEntry).map(Map.Entry::getKey)
                    .map(CPropertyInfo::getSchemaComponent).map(ParticleImpl.class::cast).map(ParticleImpl::getTerm)
                    .map(XSElementDecl.class::cast).map(XSElementDecl::getType);
                Optional<String> targetNamespace = xsType.map(XSDeclaration::getTargetNamespace)
                    .filter(v -> v.equals(this.namespace));
                if (targetNamespace.isPresent()) {
                    Optional<String> className = xsType.map(XSDeclaration::getName).map(annotations::get);
                    if (className.isPresent()) {
                        JFieldVar fieldImpl = (JFieldVar) getFieldImpl.get(fieldEntry.getValue());
                        fieldImpl.annotate(outline.getCodeModel().directClass(className.get()));
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Cannot annotate fields", ex);
            throw new SAXException(ex);
        }
        return true;
    }


}