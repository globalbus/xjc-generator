package info.globalbus.xjc.lombok;

import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.tools.xjc.Options;
import com.sun.tools.xjc.Plugin;
import com.sun.tools.xjc.outline.ClassOutline;
import com.sun.tools.xjc.outline.Outline;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.xml.sax.ErrorHandler;

public class LombokPlugin extends Plugin {

    public static final String OPTION_NAME = "Xlombok";
    private final Map<String, Command> commands = new HashMap<>();

    public LombokPlugin() {

        addLombokCommand("Getter", Getter.class);
        addLombokCommand("Setter", Setter.class);
        addLombokCommand("GetterSetter", Getter.class, Setter.class);
        addLombokCommand("ToString", ToString.class);
        addCommand(new EqualsAndHashCodeCommand("EqualsAndHashCode"));
        addLombokCommand("NoArgsConstructor", NoArgsConstructor.class);
        addLombokCommand("AllArgsConstructor", AllArgsConstructor.class);

        addCommand(new LombokCommand("Builder", Builder.class) {
            @Override
            public void editGeneratedClass(JDefinedClass generatedClass) {
                JDefinedClass implClass = generatedClass;
                implClass.annotate(NoArgsConstructor.class);
                implClass.annotate(AllArgsConstructor.class);
                implClass.annotate(Builder.class).param("builderMethodName", "builderFor" + implClass.name());
            }
        });

        addCommand(new Command("removeGeneratedSourceSetters", "remove Setters from JAXB generated sources") {
            @Override
            public void editGeneratedClass(JDefinedClass generatedClass) {
                removeGeneratedSourceSetters(generatedClass);
            }
        });
    }

    private void addLombokCommand(String name, Class... lombokAnnotation) {
        addCommand(new LombokCommand(name, lombokAnnotation));
    }

    private void addCommand(Command command) {
        commands.put(command.getParameter(), command);
    }

    @Override
    public String getOptionName() {
        return OPTION_NAME;
    }

    @Override
    public String getUsage() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("  -").append(OPTION_NAME).append(":  add Lombok annotations \n");
        for (Command command : commands.values()) {
            stringBuilder.append(command.getUsage()).append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public int parseArgument(Options opt, String[] args, int i) {
        String arg = args[i].trim();
        Command command = commands.get(arg);
        if (command != null) {
            command.setEnabled(true);
            return 1;
        }
        return 0;
    }

    private boolean isAbstractClass(JDefinedClass generatedClass) {
        return generatedClass.isAbstract() || generatedClass.isInterface() || generatedClass.isAnonymous();
    }

    @Override
    public boolean run(Outline outline, Options options, ErrorHandler errorHandler) {
        // for each generated classes
        for (ClassOutline generatedClassOutline : outline.getClasses()) {
            JDefinedClass generatedClass = generatedClassOutline.implClass;
            if (!isAbstractClass(generatedClass) &&
                !generatedClass.fields().isEmpty()) {
                for (Command command : commands.values()) {
                    if (command.isEnabled()) {
                        command.editGeneratedClass(generatedClass);
                    }
                }
            }
        }
        return true;
    }

    private void removeGeneratedSourceSetters(JDefinedClass generatedClass) {
        List<JMethod> setters = new ArrayList<>();
        // find methods to remove
        for (JMethod method : generatedClass.methods()) {
            if (method.name().startsWith("set")) {
                setters.add(method);
            }
        }

        // remove methods
        for (JMethod method : setters) {
            generatedClass.methods().remove(method);
        }
    }
}