package info.globalbus.xjc.lombok;

import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JDefinedClass;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EqualsAndHashCodeCommand extends Command {
    EqualsAndHashCodeCommand(String name) {
        super(name, "add Lombok @" + name + " annotation");
    }

    @Override
    public void editGeneratedClass(JDefinedClass generatedClass) {
        final JAnnotationUse annotate = generatedClass.annotate(EqualsAndHashCode.class);
        if (!generatedClass._extends().isAssignableFrom(generatedClass.owner().directClass("java.lang.Object"))) {
            annotate.param("callSuper", true);
        }
    }
}