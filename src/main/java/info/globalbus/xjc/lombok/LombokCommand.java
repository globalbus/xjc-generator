package info.globalbus.xjc.lombok;

import com.sun.codemodel.JDefinedClass;
import java.lang.annotation.Annotation;

public class LombokCommand extends Command {

    private final Class<? extends Annotation>[] lombokAnnotations;

    LombokCommand(String name, Class<? extends Annotation>... lombokAnnotations) {
        super(name, "add Lombok @" + name + " annotation");
        this.lombokAnnotations = lombokAnnotations;
    }

    @Override
    public void editGeneratedClass(JDefinedClass generatedClass) {
        for (Class<? extends Annotation> lombokAnnotation : lombokAnnotations) {
            generatedClass.annotate(lombokAnnotation);
        }
    }
}